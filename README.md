![](https://gitlab.com/magenta-cloud/cicd/badges/master/pipeline.svg)

# Magenta CI/CD Security

The goal of this project is to collect security best practices to be used by the Magenta Cloud Community in Continuous Integration / Continuous Delivery workflows.

This repository holds source files for building [Magenta CI/CD Security](https://magenta-cloud.pan-net.cloud/) website.

## Technology Stack

- The [project web page](https://magenta-cloud.pan-net.cloud/) is rendered using [Hugo](https://gohugo.io/) and Gitlab pages.
- For Hugo theme we are using [Dot theme](https://themes.gohugo.io/dot-hugo-documentation-theme/).
- The SSL certificate is obtained automatically from Let's Encrypt (LE) using [Gitlab Pages integration with LE](https://docs.gitlab.com/ee/user/project/pages/custom_domains_ssl_tls_certification/lets_encrypt_integration.html).

## Getting Started with Local Development

To test web page rendering locally do:

1. Install [Hugo](https://gohugo.io/getting-started/installing/) on you machine (ideally use same version as used in [.gitlab-ci.yml](https://gitlab.com/magenta-cloud/cicd/-/blob/master/.gitlab-ci.yml#L2))

2. Clone repo
   ```bash
   $ git clone git@gitlab.com:magenta-cloud/cicd.git && cd cicd
   ```

3. Run Hugo server locally:
   ```bash
   $ hugo server
   ```

4. In your favorite browser visit http://localhost:1313/ to preview local copy of the contents.


## How to Contribute

To start contributing to the project and to understand workflow please see [How to Contribute](https://magenta-cloud.pan-net.cloud/contribute/).

In short you should follow these steps:

1. Choose from the list of existing topics or [add your topic](https://magenta-cloud.pan-net.cloud/contribute/adding_topics/) to the issue backlog

2. Follow [General workflow](https://magenta-cloud.pan-net.cloud/contribute/general_flow/)

3. Request [Code Review](https://magenta-cloud.pan-net.cloud/contribute/code_review/)


### Adding a page to the project

If you have `hugo` command installed you can use `hugo new` command to create new file and it will be prepoluted with template, all you need to do is customize it.

1. Go to git repo je cloned:

  ```
  $ cd cicd/
  ```

2. Add new post with the file name that you want to be the title of the topic e.g.:
   ```
   $ hugo new get_started/cd/iac/managing_secrets/managing-secrets-using-gitlab-variables.en.md
   ```
   This will create post with title `"Managing Secrets Using Gitlab Variables"`


If you don't have `hugo` installed locally you can do following:

1. Under correct section create Markdown file with name that is short description of your topic (e.g. `hardening-os.en.md`)

2. Paste contents of the following template into the new file and edit:

   ```
   ---
   title: "Title of the new topic "
   # date when content was last modified
   lastmod: 2020-07-08
   # weight determines order of the posts in the side menu
   weight: 1
   draft: false
   # search related keywords
   keywords: ["keywords", "related", "to", "your", "topic"]
   ---
   

   ### Security problem

   _Write here about security problem/dilemma you are trying to address._

   ### Security control proposal

   _Write here about possible solutions to a problem/dilemma stated above._


   ### Reference implementation

   _Add here a relevant implementation from reference documentation,
   but make sure it's security compliant.
   Sometimes reference documentation implementations are not secure,
   with security being addressed in a footnote._

   ### Practical implementation

   _Add here a practical implementation that is self contained.
   Make sure you are not sharing any private or confidential information._

   ```


## Scrum

We organize our work and contribution in Agile/Scrum methodology. First of all, we defined the two-weeks sprints:

https://gitlab.com/magenta-cloud/cicd/-/milestones

**Anybody** from the Community is able to create a task. Each task should be well-defined and be in the scope - solving any of the Security dilemmas defined here:
https://magenta-cloud.pan-net.cloud/get_started/

The list of task is here:
https://gitlab.com/magenta-cloud/cicd/-/issues


After a task is created, the Scrum Master moves the task into a specific column - upon agreement - in the Scrum board:
https://gitlab.com/magenta-cloud/cicd/-/boards

We have 7 columns in the board:
- Open: Backlog, where each task is put in the first place after creation. Each task will be analysed whether it fits the scope, is well enough defined etc.
- To Do: Tasks which are **ready** to be moved to "Doing (xyx)" columns
- Doing: Any task which nature does not fit within the following 3 columns 
- Doing (Security Control): e.g. Security Test, Architecture pattern, Compliance Check
- Doing (Implementation Example): e.g.Technology, Code Snippet, Concept Blueprint, etc.
- Doing (Consumable Reference): within the DTAG Group, for instance in Pan-Net Product
- Closed

All tasks should within the sprint should be finished before the sprint ends. If it's not the case, they will be moved to the following sprint. The granularity (size) of the tasks should be similar, so that we keep the **velocity** of the sprint at stable or, in the ideal case, increasing level.

Anybody can check the current status sprint status by viewing **Burndown Chart** for specific sprint, i.e. for Sprint 2 the Burndown Chart is:
https://gitlab.com/magenta-cloud/cicd/-/milestones/2

