---
title: "Frequently Asked Questions"
draft: false
---

{{< faq "I'm not new to the Community, how can I cooperate?" >}}
First, you'll be added to our Magenta Cloud SLACK workspace - an easy and efficient way how to interact with all community members. We'll invite you also to our GitLab  [workspace](https://gitlab.com/magenta-cloud/cicd) where the actual solutions will be discussed and developed. You will have access to the TDAG's YAM [page](https://yam.telekom.de/groups/magenta-cloud-community) where we share our progress internally with our DTAG colleagues and in the same time we do have our public page [link](https://magenta-cloud.pan-net.cloud/) to share our work with broader community. Be welcome to Magenta Cloud CI/CD stream!
{{</ faq >}}

{{< faq "I want to contribute, how should I do that?" >}}
First, please visit our Introduction [page](https://magenta-cloud.pan-net.cloud/get_started/introduction/) to have an overview on the initiative itself.  After you're familiar with topics, please visit the How to Contribute [page](https://magenta-cloud.pan-net.cloud/contribute/). You can contribute either by implementing a solution, following the [General Flow](https://magenta-cloud.pan-net.cloud/contribute/general_flow/), by adding a [new topic](https://magenta-cloud.pan-net.cloud/contribute/adding_topics/) or [reviewing the code](https://magenta-cloud.pan-net.cloud/contribute/code_review/). As an observer, feel free to comment on user stories, feedback solutions and discuss the issues. In any time don't hesitate to Slack us in our Magenta Cloud workspace and we'll be happy to help you around! 
{{</ faq >}}

{{< faq "How does the organisation look like?" >}}
The complete list of current CI/CD Stream stakeholders and members can be viewed [here](https://yam.telekom.de/docs/DOC-698841). 
We're having a regular one-hour weekly Community call, every Friday at 9:00. On top of it, we're having ad-hoc technical sessions where we discuss current work, proposals and solutions on a hight expert level.
{{</ faq >}}


<hr class="mt-5 mb-5">

{{< faq "What is a CICD pipeline?" >}}
CI/CD is a set of methods that enables application developers to deliver code changes more frequently to customers through the use of automation.
You can learn [here](https://devops.pan-net.cloud/intro)
{{</ faq >}}

{{< faq "How to Add Security Into the CICD Pipeline?" >}}
You should ensure that your builds are clean and secure before they are deployed. You can accomplish this by implementing security scanning across your CI/CD pipeline!
Uses existing messaging channels for alerts on build security quality issues.  Provides meaningful information to the developer through preferred channels, not just, "Hey, your build failed because 'Computer says no...' " The more detailed the information, the faster the developer can likely diagnose the problem and get the build back on track.
{{</ faq >}}

{{< faq "How should I analyse my Pipeline?" >}}
Bear in mind security triad: Confidentiality, Integrity, and Availability.  
Gather as much information about:
-consumed/produced data
-the regulatory requirements
-locations that data will traverse
Make sure you do the cyberthreat modeling.
[More..](https://magenta-cloud.pan-net.cloud/get_started/ci/plan/analyse/)
{{</ faq >}}

{{< faq "Difference between security of the pipeline and security in the pipeline?" >}}
Per CodeDx, organizations can ensure the security of their CI/CD pipeline with relative ease so long as they’ve followed the best practices for DevSecOps. They should specifically require authentication for anyone to push changes to the CI/CD pipeline, implement login tracking and confirm that builds reside on secure servers only.

In contrast to security of the pipeline, security in the pipeline is a bit more involved. Organizations should focus on several best practices like SAST, peer code reviewes, unit testing, functional security testing, security automation.
{{</ faq >}}

{{< faq "Why is documentation crucial? " >}}
It is one of fundamentals to have its security-relevant data noted down somewhere getting ready to react swiftly and efficiently if something goes wrong (e.g. during a system failure or a security breach).
Security-relevant data shall be precise, relevant and up-to-date, it should be coming from a product’s resources directly (such as source codes, configuration files, etc.). [More..](https://magenta-cloud.pan-net.cloud/get_started/ci/plan/documentation/)
{{</ faq >}}








