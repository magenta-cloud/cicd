---
title: "Verify"
date: 2018-12-29T11:02:05+06:00
lastmod: 2020-07-08T08:41:42
weight: 1
draft: false
# search related keywords
keywords: ["deployment", "verify", "automation", "production", "security", "CICD", "devSecOps", "problem", "community", "cloud"]
---

In this section, we are analyzing existing strategies on how to make sure the application binaries, packages or even OS images are genuine and not manipulated.  
