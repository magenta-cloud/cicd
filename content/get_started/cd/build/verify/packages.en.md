---
title: "Checking integrity of packages"
# date when content was last modified
lastmod: 2020-11-16T15:46:17+01:00
# weight determines order of the posts in the side menu
weight: 2
draft: false
# search related keywords
keywords: ["security", "integrity", "packages", "cicd", "problem", "configruation", "repository", "signature", "installing", "hash", "control", "proposal"]
---


### Security problem

Various operating systems provide package managers for easy installation, configuration and upgrade of software.
Since the repositories containing packages are usually located on remote server.
This exposes system to a potential man-in-the-middle attack, where attacker can plant malicious package with the same name as the official one.

Some software can also come in form of the package that is distributed independently of the official repositories.
In this case an administrator has to install package manually and make sure the package wasn't tampered with.

### Security control proposal

To avoid installing malicious software it's recommended to do the following, when installing packages:

  * Install software only from the official repositories.
  * Make sure your package manager has up to date public key used to verify package(s).
  * Make sure your package manager verifies both hash and signature of the package (e.g. verification is not disabled).


If you plan to install custom package(s) from third party vendor(s) make sure:

  * You trust the vendor to deliver safe and secure software.
  * Vendor provides cryptographic hash and public key used to verify the package.
  * Make sure the exchange is done over secure channel (e.g. vendor publishes packages, hashes, and public keys on website which uses HTTPS).


### Reference implementation

#### RHEL/Centos/Fedora

Red Hat distributions use `rpm` package manager. YUM (DNF on Fedora) are frontends made to ease RPM management.

To import public key to be used by `rpm`:
```bash
~$ rpm --import <file-with-public-key>
```

To verify package with one of the imported keys:
```bash
~$ rpm -K <package-name>.rpm
```

For YUM/DNF when you add repository to `/etc/yum.repos.d/`, the configuration file will contain URL or path to GPG key used to sign all packages in that repository.

```bash
~$ cat /etc/yum.repo.d/fedora.repo
[fedora-source]
name=Fedora $releasever - Source
#baseurl=http://download.example/pub/fedora/linux/releases/$releasever/Everything/source/tree/
metalink=https://mirrors.fedoraproject.org/metalink?repo=fedora-source-$releasever&arch=$basearch
enabled=0
metadata_expire=7d
repo_gpgcheck=0
type=rpm
gpgcheck=1
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-fedora-$releasever-$basearch
skip_if_unavailable=False
```
