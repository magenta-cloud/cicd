---
title: "Build"
date: 2018-12-29T11:02:05+06:00
lastmod: 2020-07-08T08:41:42
weight: 2
draft: false
# search related keywords
keywords: ["deployment", "automation"]
---

Subsection about building application will go through secure way to package, ship and verify packages, images, and binaries.
Furthermore, deployment of the application is described.
