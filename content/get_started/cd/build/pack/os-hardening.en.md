---
title: "Pre Hardened OS Images"
lastmod: 2020-09-15T14:00:21+02:00
weight: 1
draft: false
keywords: ["deployment", "stage", "automation", "production", "deploy to prod", "security", "CICD", "devSecOps", "problem", "community", "cloud", "hardening", "images"]
---

### Security Problem

When relying on Infrastructure as a Service (IaaS) as the main service used in a cloud environment servers are spawned and destroyed on demand from standard OS-Images.
The images provided by the providers are normally not compliant to our internal requirements.
The hardening of such images by hand is error prone and not efficient especially when it is done on a per project or system basis.
A configuration by hand always results in a configuration drift across all existing projects.
This drift introduces a security-risk that is incalcuable nor measurable.

### Security control proposal

Establish a mechanism to automate hardening and build hardened images that are provided to all the projects.
It is important to rely on a specified set of security requirements and verify the results.
A set of tests to verify the compliance of a system should also be provided.

### Reference Implementation

There exist several reference implementations that are built against different Requirement-Sets.
These frameworks can be used to implement a solution to build hardened images with a tool like (Packer)[https://www.packer.io/] on top of most of the cloud technologies currently in use.

#### Telekom IT Security Automation Framework

The Telekom IT Security Automation Framework is developed within Deutsche Telekom IT.
It provides [Ansible](https://www.ansible.com/) roles which harden the SSH service and the Linux systems according to the requirements from Deutsche Telekom Security.
The roles are made available on [GitHub](https://github.com/telekom/tel-it-security-automation).
We also provide a testing framework in that repository which verifies the compliance to Deutsche Telekom Securitys requirements and provides a pre-filled SoC-List for internal use.

#### DevSec Hardening Framework

Another one is the [Dev-Sec](https://dev-sec.io) project which provides hardening for several Linux and Windows flavors as well as middleware and applications.
In addition they provide a local testing environment based on [Kitchen](https://kitchen.ci/) and tests for their baseline based on [InSpec](https://github.com/inspec/inspec).
The applied settings are based on various security requirements and are partially following Deutsche Telekom Security's Reqiurments.
A full implementation to build a pipeline is missing but the hardening-automation is given.
The framework provides configuration automation for [Chef](https://www.chef.io/), [Puppet](https://puppet.com/) and Ansible.

### Practical Implementation

Within Deutsche Telekom IT we have a practical implementation of a project that automatically builds and tests hardened machine images on several clouds.
Therefore we use the tool Packer to bake the hardening onto the images made available by the cloud provider.
To verify and test our roles we are using [Molecule](https://molecule.readthedocs.io/en/latest/), a framework that can be used to verify Ansible playbooks on several infrastructures and platforms at one time.
The molecule configurations to test the roles will be published on GitHub in the future.
We are currently verifying the functionality of our roles and provide our hardened images on AWS and an OpenStack based cloud.
In addition Deutsche Telekom IT provides hardened EKS worker nodes that can be used with a terraform template within our environments.
As soon as the configuration for molecule is published Deutsche Telekom IT will release a more detailed documentation on how this framework can be used.
