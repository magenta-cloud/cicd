---
title: "Checking IaC compliance"
# date when content was last modified
lastmod: 2020-08-13
weight: 3
draft: false
# search related keywords
keywords: ["IaC", "compliance", "code checking", "static code analysis", "security", "proposal", "checkov", "terraform", "BDD", "Python", "Kubernetes", "Clodformation", "compliance"]
---

### Security problem

Infrastructure as Code (IaC) tools are an interface to build infrastructure on different cloud providers.
Cloud providers aim to give user as much freedom as possible, which comes at a cost. 
Due to many configuration options, it's easy to overlook security implications that certain deployment might have. 


### Security control proposal

To make sure your code is following security and compliance best practices, do following:  
* use one or more code compliance checkers available for your IaC tools 
* make compliance checks part of the pipeline as early as possible  
* fix all errors and warnings that checkers report
* make sure checkers are up to date  


### Reference implementation


{{% tabs %}} 

  {{% tab "terraform-compliance bash example" %}}
```bash
~$ pip install terraform-compliance
~$ cd your/terraform/repo/dir/
~$ mkdir features/
~$ cat <<EOF >feature/test.feature
Feature: Check security groups settings
    Scenario: Make sure security group isn't allowing connections from everyone
        Given I have openstack_networking_secgroup_rule_v2 defined
        Then it must contain remote_ip_prefix
        And its value must not be 0.0.0.0/0
EOF 
~$ terraform plan -out=plan.out
~$ terraform-compliance -f features/ -p plan.out

```
  {{% /tab %}}

  {{% tab "checkov bash example" %}}
```bash
~$ pip install checkov
~$ checkov -d /path/to/iac/code

```
  {{% /tab %}}


{{% tabs %}} 


#### terraform-compliance

`terraform-compliance` is checker written in Python, that uses Behaviour Driven Development (BDD) to define check scenarios. 
The example above shows how to define simple `Feature` to test if remote IP prefix for security group isn't too open.
For more details on how to write features see: https://terraform-compliance.com/pages/bdd-references/.

#### checkov

Excerpt from documentation:
> Checkov is written in Python and aims to simplify and increase the adoption of security and compliance best practices that prevent common cloud misconfiguration. Its scans adhere and implement common industry standards such as the Center for Internet Security (CIS) Amazon Web Services (AWS) Foundations Benchmark.

`checkov` covers security best practices for AWS, Azure and Google Cloud. 
The tool is opensource, so adding missing checks is possible.
The fact that it can scan Terraform, Clodformation and Kubernetes code makes it very good choice for hybrid deployments.
