---
title: "Creating VM"
# date when content was last modified
lastmod: 2020-07-28
weight: 3
draft: false
# search related keywords
keywords: ["security", "creating VM", "cloud", "automation", "build", "virtual", "machine", "IaC", "Terraform", "OpenStack", "continous", "delivery"]
---


### Security problem

When creating VMs in a cloud using IaC tools, some of the information needed to build a VM is sensitive and MUST NOT be committed to a version control system.


### Security control proposal

* Make sure you read IaC tool information on how to handle sensitive information.

* Store credentials in trusted back-end as close to your automation tools as possible.  

* Add credentials to environment only during build process (don't hard code them into source files).

* Use static checkers for your automation tool to make sure your code doesn't contain any secrets.


### Reference implementation

The following examples are taken from tool's documentation examples and modified to fit into this security context.
The values of attributes need to be modified to correspond with values in your cloud.

{{% tabs %}} 

  {{% tab "terraform openstack" %}}
```yml
	resource "openstack_compute_instance_v2" "basic" {
	  name            = "simple_vm"
	  image_name      = "fedora_31"
	  flavor_name     = "m1.small"
	  key_pair        = "vm_key_pair"
	  security_groups = ["default"]
	  network {
	    name = "new_vm_network"
	  }
	}
```
  {{% /tab %}}

  {{% tab "heat openstack" %}}
```yaml
	heat_template_version: 2017-09-01
	description: Simple template to deploy a single VM
	resources:
	  my_instance:
	    type: OS::Nova::Server
	    properties:
	      key_name: vm_key_pair
	      image: fedora_31
	      flavor: m1.small
```
  {{% /tab %}}


{{% tabs %}} 

#### Terraform Openstack example

In the Terraform example above it's important to create Openstack key pair `vm_key_pair` beforehand (e.g. using `openstack` CLI).
If you use [Openstack keypair resource](https://www.terraform.io/docs/providers/openstack/r/compute_keypair_v2.html) to create key pair, the private key will end up unencrypted in terraform state file.
The private key should be stored somewhere safe and be easily accessible when other automation tools need to use it.

