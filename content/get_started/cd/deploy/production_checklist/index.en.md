---
title: "Production Readiness Checklist"
# date when content was last modified
lastmod: 2021-01-04
weight: 4
draft: false
# search related keywords
keywords: ["security", "production", "readiness", "checklist"]
---


### Security problem

The production readiness checklist is a safety tool that enables your team to quickly check and align what is not properly covered in the production ready system. The checklist is to be used as a guide for a safe enrollment of a system in a production ready state.  

### Security control proposal  

Below is an example of the production readiness checklist for a software deployment:  

Item  | Description | Comments | Status
--- | --- | ---  | ---
Logging | *Onboarding to SIEM system* | | DONE
Ops Monitoring and Alerting | *Onboarding to Monitoring / Telemetry system* |  | ?
Incident Management | *Onboarded to Incident Management e.g. Pager Duty?* |  | ?  
Documentation for Public | *At least "User Guide"* |  | ?
DevOps pipelined | *3 environments (DEV, STAGE, PROD)* |  | ?
Testing | *Basic automated test between DEV/STAGE and STAGE/PROD - it means propagation to PROD is without or minimum human intervention* | | ?
Deployment | *Automated Pipelines for deployment (keywords: infrastructure-as-a-code, configuration-as-a-code, immutable and reproducible deployment* |  | ?
Disaster Recovery | *Do we backup all data? Are we able to recover from a disaster (e.g. data loss)?* |  | ?
PSA and Pen test |  |  | ?
