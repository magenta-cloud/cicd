---
title: "Runtime"
date: 2018-12-29T11:02:05+06:00
lastmod: 2020-07-08T08:41:42
weight: 4
draft: false
# search related keywords
keywords: ["running", "automation", "runtime"]
---

In this subsection you will find information about dynamic application testing and how to use observability to increase security of applications.
