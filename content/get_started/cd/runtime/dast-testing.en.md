---
title: "Dynamic Application Security Testing (DAST) of an API"
# date when content was last modified
lastmod: 2020-07-08
weight: 1
draft: false
# search related keywords
keywords: ["DAST", "security", "API", "testing", "scans", "automation"]
---

### Security problem

By exposing an API to the outside world, you will inevitably receive unwanted attention from attackers.
Any non-trivial application will have many configuration options, which means errors are easy to make.
Unit and integration tests can only do so much to test overall security of a final product setup.
Once you deploy your application to a staging environment, you should try to scan your API for known vulnerabilities and misconfiguration.

### Security control proposal

Run automated testing tools, similar to those that attackers would use..
Add custom tests alongside standard tests, that come with most DAST tools.
Automated scans should act as a final safety net, against vulnerable app being exposed to the world.

### Reference implementation

{{% tabs %}} 

  {{% tab "ZAP docker" %}}
  ```bash

  ~$ # ZAP Baseline scan
  ~$ docker run -t owasp/zap2docker-stable zap-baseline.py -t https://www.example.com
  ~$ # ZAP Full scan
  ~$ docker run -t owasp/zap2docker-stable zap-full-scan.py -t https://www.example.com
  ~$ # ZAP API scan
  ~$ docker run -t owasp/zap2docker-stable zap-api.scan.py -t https://www.example.com/openapi.json
  
  ```
  {{% /tab %}}

{{% /tabs %}} 


#### Zed Attack Proxy - ZAP

ZAP is web app scanner made by [OWASP](https://owasp.org/). 
It's free and open source project.

You can use ZAP as a desktop app, but also for automation.
As the examples above show, you can use Docker container prepared by OWASP to perform automated Baseline, Full, and API scans.

### Practical implementation

 
{{% tabs %}} 

  {{% tab "ZAP baseline" %}}
  ```yaml
  stages:
    - staging_tests
  appscan_baseline:
    stage: staging_tests
    image: artifactory.tools.in.pan-net.eu/net_mon-images-docker-local/docker-images/docker-sec-zaproxy-stable
    variables:
      TARGET: "https://application.pan-net.eu"
    before_script:
      - mkdir -p /zap/wrk
    script:
      - /zap/zap-baseline.py -P 8080 -t $TARGET -m 10 -a -w report.md -r report.html -g config.conf || true
      - cp /zap/wrk/report.* $CI_PROJECT_DIR
    only:
      refs:
        - master
      variables:
        - $ACTION == "appscan"
    artifacts:
      name: appscan_report_pipeline_$CI_PIPELINE_ID
      paths:
      - report.md
      - report.html
      expire_in: 48 hour
  ```

  {{% /tab %}}

  {{% tab "ZAP full scan" %}}
    ```yaml
    stages:
      - staging_tests
    appscan_fullscan:
      stage: staging_tests
      image: artifactory.tools.in.pan-net.eu/net_mon-images-docker-local/docker-images/docker-sec-zaproxy-stable
      variables:
        TARGET: "https://application.pan-net.eu"
      before_script:
        - mkdir -p /zap/wrk
      script:
        - /zap/zap-full-scan.py -P 8080 -t $TARGET -m 10 -a -w report.md -r report.html -g config.conf || true
        - cp /zap/wrk/report.* $CI_PROJECT_DIR
      only:
        refs:
          - master
        variables:
          - $ACTION == "appscan"
      artifacts:
        name: appscan_report_pipeline_$CI_PIPELINE_ID
        paths:
        - report.md
        - report.html
        expire_in: 48 hour
    ```
  {{% /tab %}}

  {{% tab "ZAP API scan" %}}
    ```yaml
    stages:
      - staging_tests
    appscan_api_scan:
      stage: staging_tests
      image: artifactory.tools.in.pan-net.eu/net_mon-images-docker-local/docker-images/docker-sec-zaproxy-stable
      variables:
        TARGET: "https://application.pan-net.eu/openapi.json"
      before_script:
        - mkdir -p /zap/wrk
      script:
        - /zap/zap-api-scan.py -P 8080 -t $TARGET -m 10 -a -w report.md -r report.html -g config.conf || true
        - cp /zap/wrk/report.* $CI_PROJECT_DIR
      only:
        refs:
          - master
        variables:
          - $ACTION == "appscan"
      artifacts:
        name: appscan_report_pipeline_$CI_PIPELINE_ID
        paths:
        - report.md
        - report.html
        expire_in: 48 hour
    ```

  {{% /tab %}}
{{% /tabs %}} 

#### ZAP examples

The ZAP examples above show all 3 types of automated scans available via ZAP docker image.
The jobs are set to run during testing of the stage deployment.

The reports from the scripts are set to be stored as a pipeline artifacts, that will expire in 48 hours.
Variable called `ACTION` needs to be defined and have value equal to `appscan`, for the jobs to run.
