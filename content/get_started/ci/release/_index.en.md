---
title: "Release"
date: 2020-12-17T11:02:05+06:00
weight: 4
draft: false
# search related keywords
keywords: ["release", "binaries", "packages", "security", "cicd", "cloud", "community", "dilemmas", "control", "proposal", "goreleaser"]
---

### Security problem

When releasing your work, whether it is application binary, image or a package, you must make sure that people fetching your work can be sure, that no one in the supply chain tampered with your work.
Even if the servers, from which you are serving released work, are under your control, you need to provide a way to verify integrity of work released by you.

For anyone to be able to verify your work just as we documented in the [Verify](/get_started/cd/build/verify) topic, you must provide a file hash and ideally sign with your private GPG key.

### Security control proposal

When releasing binaries, packages or images, make sure you provide a file with calculated hashes of your files (don't use SHA1 or MD5 hashes as they are considered broken). 
Distribute your public key used for signing to a publicly trusted keyserver or some other place, that is not hosted on your infrastructure.
This way, even if your servers get compromised users can download public key and verify if the code they are downloading were really signed by you.

### Reference implementation

#### GoReleaser

For automating releasing of projects written in Golang, the [GoReleaser](https://goreleaser.com/intro/) tool exists.
With this tool you can easily build your application for various architectures. And then sign and publish your binaries on Github or Gitlab.

Check out project's [Quick Start Guide](https://goreleaser.com/quick-start/).
