---
title: "Plan"
date: 2018-12-29T11:02:05+06:00
lastmod: 2020-12-15T08:41:42
weight: 1
draft: false
# search related keywords
keywords: ["planing", "plan", "collaboration", "security", "cicd", "cloud", "community", "dilemmas", "control", "proposal"]
---

Within planing subsection various topics are discussed. 
You will find here analysing application requirements from security point of view.
Also baking security into your product right from the design phase is discussed.
The topic about security and documentation will make case for effectively keeping documentation up-to-date as a form of security measure.
