---
title: "Security documentation"
date: 2018-12-29T11:02:05+06:00
lastmod: 2020-11-06T18:00:00
weight: 2
draft: false
# search related keywords
keywords: ["documentation", "chatops", "psa", "security", "automatic", "cicd", "cloud", "community", "dilemmas", "control", "proposal"]
---

### Security problem

While a product is being developed and/or maintained, it is one of fundamentals to have its security-relevant data noted down somewhere getting ready to react swiftly and efficiently if something goes wrong (e.g. during a system failure or a security breach). Such data could be a logical addresses, communication protocols, point of exposure to an external networks, firewall rules, encryption algorithms, etc.

It might happen, that even security documentation is covered pretty well (nicely written, a lot of information), not necessarily it contains only precise and targeted information. Vice versa, even it may include a necessary information only, sometimes a more descriptive data around could be more than helpful. Moreover, even a product may be documented very well, like in a good balance of quality and quantity, sometimes it may suffer from the obsolescence.

### Security control proposal

As a security-relevant data shall be precise, relevant and up-to-date, it should be coming from a product's resources directly (such as source codes, configuration files, etc.).

During a development/operation stages of a product, it might be observed for a changes/deltas over security-relevant data. These changes could be then pushed into a repository of documentation either automatically on real-time basis (machine-driven) or by means of notifying to a product's owner/manager to write it down manually (human-driven).

While using a version control system for the overall automation of product's development/operation, it is wise to re-use the same environment also for documenting. A cycle for documenting could be hooked on existing product's CI/CD routines and to collect the changes. The other option (less computationally demanding) could be to manually evaluate the changes and adjust documentation accordingly.

Moreover, good security documentation should be easy to read and understand for both, machine and human. Therefore it might be useful to select a right format easy to get processed by both, such as plain-text-formatting syntax would be.

What is also important here, since these data are mostly classified as highly sensitive data, it is crucial to follow main security principles such as confidentiality and integrity and availability. Examples for proper measures having in place should include, but not limited to, a proper encryption, hashing, digital signatures, digital certificates, intrusion detection systems, auditing, version control, strong authentication mechanisms and access controls, redundancy, backup, disaster recovery plans, software patching, system upgrades as well as denial-of-service protections.

While using a version control system also for documenting, it might be evaluated how far it complies the above.


### Reference implementation

For reference implementation see [Hugo implementation under Technical documentation](../technical#hugo)

### Practical implementation

**PSA-bot**

[Pan-Net PSA-bot product](http://security.docs.tools.in.pan-net.eu/PSAbot/) represents a chat-ops-driven and machine-way implementation of the DTAG privacy and security process into a whole life-cycle of a product. Basically, a product owner/manager is talking to the Slack/PSA-bot by giving a commands to pass through a several stages for whole documenting as follows:
- initial classification (how critical a product is from a handling with sensitive data perspective);
- insert the markdown templates into a product's Gitlab repository for future answering of all security-relevant technical data;
- applying of DTAG technical security and privacy requirements (as Gitlab issues);
- define action plan to cover any open / not yet resolved points;
- (conditional) approving / rejecting;
