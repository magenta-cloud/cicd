---
title: "Design"
date: 2018-12-29T11:02:05+06:00
lastmod: 2020-07-08T08:41:42
weight: 2
draft: false
# search related keywords
keywords: ["analyse", "analyze",  "security", "cicd", "cloud", "community", "dilemmas", "control", "proposal", "design", "fail safe", "separation of duties", "complete mediation", "least common mechanism", "weakest link", "OWASP"]
---

### Security problem

Not properly designing application or system from the beginning can cause the problem down the road.
Design can change over time and the way design documentation is maintained should make changes easy and collaborative.

### Security control proposal

Design discussions should be open and accessible to anyone who is part of the team.
Use [Slack](https://slack.com) or [GitLab](https://gitlab.com)/[Github](https://github.com) issues to achieve this.

Maintain design documentation within your versioning system. Use for example static website generator to ([see Technical documentation section](../documentation/technical/) for example how to use [Hugo](https://gohugo.io))

Follow the [OWASP's Secure Design Principles](https://github.com/OWASP/DevGuide/blob/master/02-Design/01-Principles%20of%20Security%20Engineering.md#secure-design-principles):

 * **Defense in Depth** - have multiple layers of security (e.g. expose DB only on internal network, connect to DB using TLS, encrypt data in database and encrypt disk on which data are stored).
 * **Fail safe** - if system/application fails, it should do so to the safe defaults (e.g. error shouldn't dump customer data, login fail shouldn't say if username or password were wrong).
 * **Least privilege** - give minimum access rights necessary to perform a task at hand.
 * **Separation of duties** - don't allow single component to have permission to access everything in the system.
 * **Economy of mechanism** - Keep It Simple, Stupid (KISS), i.e. don't overcomplicate the design or add unnecessary components.
 * **Complete mediation** - every time the task is done, check if entity has correct authorization to do it.
 * **Open design** - the knowledge of the design shouldn't be enough to compromise the security of the system.
 * **Least common mechanism** - shared resources should not share common access mechanism.
 * **Psychological acceptability** - make security mechanisms suer friendly and transparent.
 * **Weakest link** - your software is only secure as its weakest component. 
 * **Leveraging existing components** - this is similar to Economy mechanism, and means reuse existing components if you can, so attack surface doesn't increase.


The above principles can be applied to the application code or to the complete system design.
The CI/CD pipelines design should be viewed through the lens of the above stated principles as well.  
