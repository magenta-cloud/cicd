---
title: "Integration Test"
date: 2018-12-29T11:02:05+06:00
lastmod: 2020-12-30T12:41:42
weight: 4
draft: false
# search related keywords
keywords: ["integration", "test", "security", "cicd", "cloud", "community", "dilemmas", "control", "proposal"]
---

### Security problem

When building complex applications, it's not enough to test only its individual components.  
Rather, you have to combine components together as they form your application and test them against a well specified functional requirements.

### Security control proposal

It's a good start to have a three environments - `DEV`, `STAGE` and `PROD` as a part of the production readiness checklist. The development environment is used for a fast iterating during new development. Disruption and incompatibility are expected here. Contrary, staging environment should mirror the actual production with all the conservative security settings and resources configured as we expect them in production. If possible, the only difference between staging and production environment should be testing data.  
Staging environment than can be leveraged for end to end integration testing.
You can define one single test that will cover the most critical part of the whole system and start by implementing it. In case of a database, it can be writing key to a database successfully. In case of the Radius, it can be a successful login against the Radius instances. Some of those integration tests can be later reused in the production monitoring.

Regarding the security tests, with all security features enabled, you can perform DDoS attacks, or try SQL injections against the staging environment without the fear of breaking production environment.
