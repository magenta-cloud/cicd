---
title: "Test"
date: 2020-12-15T11:02:05+06:00
lastmod: 2020-07-08T08:41:42
weight: 3
draft: false
# search related keywords
keywords: ["testing", "test", "integration", "unittest", "security", "cicd", "cloud", "community", "dilemmas", "control", "proposal"]

---

In this section, we discuss the topic of security aware application testing.
