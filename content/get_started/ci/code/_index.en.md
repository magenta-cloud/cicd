---
title: "Code"
date: 2018-12-29T11:02:05+06:00
lastmod: 2020-07-08T08:41:42
weight: 2
draft: false
# search related keywords
keywords: ["collaboration", "tema", "collaborate", "security", "cicd", "development", "build"]
---

This subsection focuses on security measures you need to do during code development.
You will find here information how to do dependency checks, so your dependencies are up-to-date and secure.
The tools for doing static code analysis are also described.
