---
title: "Dependency Checks"
# date when content was last modified
lastmod: 2020-11-03T17:28:16+01:00
# weight determines order of the posts in the side menu
weight: 3
draft: false
# search related keywords
keywords: ["dependency", "dependency-check", "gemnasium", "xray", "jfrog", "security", "cicd", "cloud", "community", "dilemmas", "control", "proposal"]
---


### Security problem

When creating a new software, it's common to use third party libraries and dependencies.
If you are working with open source software it's not uncommon for those dependencies to have their own dependencies.
Very quickly this can spiral out of control and it can be hard to keep up with all the different vulnerabilities that are discovered across your dependency list.

This is where dependency checkers come in handy.

### Security control proposal

The idea behind the dependency checkers is to scan dependency lists of your software and compare library versions against vulnerable versions in [National Vulnerability Database - NVD](), [Common Vulnerabilities and Exposures - CVE]() or a custom database that collects from multiple online sources. 

There is multitude of solutions available. The following list is by no means definitive one.

#### JFrog Xray

Artifactory offers integration with [JFrog Xray](https://jfrog.com/xray/) and can be used to perform continuous security scanning of artifacts.
It can be used both as a tool to scan dependencies when developing software and to scan images.

XRay uses custom vulnerability database, that is populated from global XRay database maintained by JFrog.
You set up policies and rules to adhere to your internal governance standard. 
Next you define scope on which to apply this policy and set up monitoring, that will notify you about policy violation.  

Once you start pushing images to Artifactory, the XRay will scan them and report problematic dependencies along with additional information about the vulnerabilities.

#### Dependency-check

Dependency-check is an open source tool. It scans files based on file type using Analyzers.
The language support is not as extensive as with some other tools.
It can be invoked as CLI tool, as plugin in CICD tools (Jenkins, Gradle) or as part of the Maven or the Ant.

#### Gitlab Dependency Scanning

Originally GEMNASIUM was a project on it's own, but today it's part of the Gitlab offering called [Gitlab Dependency Scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/index.html).
To use the Gitlab Dependency Scanning you will need the non-free plan from Gitlab. 
But otherwise the solution works out of the box, with very little hustle needed to set it up.

Gitlab Dependency Scanner offers multiple languages and package manager. All popular languages are covered. 
You just need to include scanning template to your `.gitlab-ci.yml` file and use environment variables to configure the template.
After pipeline has finished, you can find a dependency scanning report as an artifact, which you can download and inspect.

Nice feature of this integration is that you can view vulnerabilities in the [Security Dashboard]() and decide what to do with them. Some vulnerabilities can even be automatically remediated. 


### Reference implementation

#### Dependency-check

A simple CLI example from [dependency-check documentation](https://jeremylong.github.io/DependencyCheck/dependency-check-cli/index.html).

{{% tabs %}}
  {{% tab "dependency-check" %}}
```bash
dependency-check.sh --project "My App Name" --scan "/java/application/lib"
```
  {{% /tab %}}
{{% /tabs %}}


#### Gitlab Dependency Scanning


{{% tabs %}}
  {{% tab "dependency-check" %}}
```yml
include:
  - template: Dependency-Scanning.gitlab-ci.yml
```
  {{% /tab %}}
{{% /tabs %}}

### Practical implementation
