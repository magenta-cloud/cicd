---
title: "CI Security"
date: 2018-12-29T11:02:05+06:00
lastmod: 2020-07-08T08:41:42
weight: 2
draft: false
# search related keywords
keywords: ["induct", "instate"]
---

Within this subsection you will find security topics related to:
  * planing your application  
  * doing secure code development  
  * performing testing with focus on security  
  * releasing your code/binary/application in a secure manner  
