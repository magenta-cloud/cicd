---
title: "DT Pan-Net"
date: 2018-12-29T11:02:05+06:00
lastmod: 2020-07-08T08:41:42
weight: 2
draft: false
# search related keywords
keywords: ["pan-net"]
---

Deutsche Telekom Pan-Net, s.r.o. is an international company founded and based in Bratislava, Slovakia. It is a member of the Deutsche Telekom Group. 
The company has been building and operating the pan-European infrastructure cloud and centralize telecommunications applications production, called Pan-Net.
Deutsche Telekom Pan-Net s.r.o. in Bratislava, Slovakia was established in 2015 as a Managing Company for another 10 local Pan-Net companies in Europe which hold and run the hardware assets and serve additional functions like service operation center, test lab, data centers, etc.
![Pan-Net is a cross-border clud based telecommunication company. Its services spread from messaging, VoLTE, email and others. The focus is on availability, scalability and deployment of applications. Within the Magenta Cloud community we propose the solutions and security controls to tackle security dilemmas.](pannet.jpg)
Pan-Net represents a truly revolutionary cross-border and cloud-based telecommunication production distributed across several data centers in Europe. It moves the production logic from legacy hardware platforms into the virtual environment based on IP, Software-Defined Networking and Network Function Virtualization. 
As of 2019, Pan-Net’s services via the private cloud (like messaging, VoLTE/VoWiFi, email, etc.) have been launched in 12 countries, serving more than 50 million end customers of national operators. Being part of a bigger DT unit, International Technology and Services Delivery (ITS), Pan-Net is in a position to be part of the development and implementation of many other services.
Besides that, our experts are focusing on developing and providing cloud service offer in Infrastructure as a Service model, letting our national operators and other internal units/customers consume our cloud on demand. 
Pan-Net’s vision is a cloud-native platform. The focus is put on “Usability first” which is based on the improvement of availability, scalability, and deployment of applications. The aim is to provide a self-service oriented mechanism to consume cloud resources enabling developers and application owners to work autonomously and to decouple completely the application from the infrastructure complexity.
We are convinced that infrastructure should be transparent, automated, self-serviceable and dynamic. It is important to fill the gaps between infrastructure and developers, providing a cloud-native platform that will empower them to handle the underlying infrastructure in a simple and familiar way. A cloud-native approach will help to faster produce value while the customers can focus on their business objectives.
