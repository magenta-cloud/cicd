---
title: "About Us"
date: 2018-12-28T11:02:05+06:00
icon: "ti-info-alt"
description: "Information about the organization behind the project"
type : "docs"
---


As a Magenta Cloud Community we want to enhance the collaboration across the whole group to support a secure, simplified, scalable utilization of public cloud solutions.  

Cloud users should be able to use cloud services without loosing speed and agility (incl. confidential data subject to staying compliant in regards to legal and contractual requirements). An agreement on how to get services approved without interrupting agile development...

## Objective of our community:
- Deliver Secure CI/CD Blueprints, Snippets and Consumable Products
- It has to be generic and applicable in any cloud infrastructure or organization (cloud agnostic)

## Governance
- Our Project is following [FOSS](https://en.wikipedia.org/wiki/Free_and_open-source_software) community approach
- Asynchronous way of working
- Creation of project in GIT REPO and applying Git-Flow control
- Regular community meetings

## Roles
- Organization Master @Vladimir
- Communication Master @Lena
- Editor @Martin
- Approver: Engineers @Miro @Peter
- Contributor: Volunteering Engineers
- Observer: anyone from Security Community (within the Group)
- Steering: @Philipp, @Andreas, @Pavol

## Tooling

| Purpose | Technology/Method | Product Name | Description |
| ------ | ------ |------ |------ |
| Communication| Conferencing system| Zoom | calls and sharing |
| Communication| Chat| Slack| Provides 24/7 Central Operations for  Services, Applications, Platforms or Infrastructures covering:  Event management , Incident management , Change management , Problem management |
| Communication| Community page| YAM| Site|
| Source code | Version Control and Artefact management| Gitlab.com| REPO and Gitlab Artefacts|
| Organisation| Scrum/Kanban | Gitlab.com | Gitlab  Boards and Issue tracking Preferably KANBAN|
| Infrastructure| Cloud| Pan-net.cloud | Tenant with public FIP|
| Infrastructure| CI| Gitlab Runner | TBD |
| Infrastructure| Runtime&Orchestration| CaaS|  K8s + Docker|
| Documentaion| Text maintenance | Markdown| |
| Documentaion| Publishing | HUGO| |
| Documentaion| Delivery | CI/CD| |
| Documentaion| Web | NGINX| |
