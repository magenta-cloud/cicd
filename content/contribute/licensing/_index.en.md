---
title: "Licensing"
lastmod: 2020-07-08T08:41:42
weight: 3
draft: false
description: "contribution and licensing"
# search related keywords
keywords: ["license"]
---

The Magenta Cloud CI/CD Security documentation is available under the Creative Commons Share-Alike 4.0 International (CC BY-SA 4.0). This license allows us to ensure that this knowledge remains free and open while encouraging contribution and authorship.

For more information regarding the license and its usage please go here: https://creativecommons.org/licenses/by-sa/4.0/