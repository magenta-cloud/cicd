---
title: "Contribution flow"
lastmod: 2020-08-19T08:41:42
weight: 1
draft: false
# search related keywords
keywords: ["flow"]
---


### Workflow for users

If you are user and you don't find a security topic you are interested in, you can add that topic to the backlog. 
In this case follow steps `1.` to `4.`.
After you submit a topic, someone will start working on it.
This person will reach out to you, if they need any clarification regarding your issue.

### Workflow for topic contributors

As a topic contributor, you will either use the full workflow described below, in case you are the person coming up with security topic and writing content.

If you will be working on an existing issue from backlog, then you should follow steps `5.` to `9.`.


### Full workflow

The general flow for contributing to CI/CD Security repository is: 
1. Choose a topic to work on based on [Introduction section](../../get_started/introduction) (or [from issue backlog](https://gitlab.com/magenta-cloud/cicd/-/issues)) 
2. Check if similar issue already exist; if not open a new issue in [Gitlab](https://gitlab.com/magenta-cloud/cicd/-/issues) 
![new-issue](new-issue.png)
3. Write issue in form of a User Story:

   ```markdown
   As a ....
   I want to  ....
   so that ....
   ```

4. Assign your self to the issue; label the issue with one of the key area tags and press Submit.  
![new-issue-create](new-issue-create.png)
5. Open merge request (MR) using `Create merge request` button (this will automatically create branch with issue number and title as branch name e.g. 1-example-issue)  
![branches](branches.png)
6. After MR is opened, assign yourself to the MR and tag it with `Doing` tag.  
7. Write a solution, a guide or a topic to markdown file(s) under subdirectory of `content/` directory. To make content uniform, please use the template below with the predefined sections.   

   ```markdown
   ---
   title: "Title of the new topic "
   # date when content was last modified
   lastmod: 2020-08-19T15:48:21+02:00
   weight: 1
   draft: false
   # search related keywords
   keywords: ["keywords", "related", "to", "your", "topic"]
   ---
   
   
   ### Security problem
   
   _Write here about security problem/dilema you are trying to address._
   
   ### Security control proposal
   
   _Write here about possible solutions to a problem/dilema stated above._
   
   
   ### Reference implementation
   
   _Add here a relevant implementation from reference documentation,
   but make sure it's security compliant.
   Sometimes reference documentation implementations are not secure,
   with security being addressed in a footnote._
   
   ### Practical implementation
   
   _Add here a practical implementation that is self contained.
   Make sure you are not sharing any private or confidential information._
   
   ```

8. Request a review from one of the reviewers by tagging them in merge request comments or adding them as an assignee to the issue (Reviewers: [Miroslav Bagľaš](https://gitlab.com/mbag-mag))
9. After review and changes, reviewer will merge branch to master. The resulting git history will look like this:  
![git-branching](git-branching.png)


### Contributing small changes such as fixes, typos

If you spot a typo or some other error, that would not be considered fundamental 
change to the existing topic, you can skip the step **3.** from the general flow.  
Label such issue as `fix` and 

